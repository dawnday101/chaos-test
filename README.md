# Chaostoolkit Experiments

This collection of experiments run tests using opensource tool Chaostoolkit.
Further details on installation, extensions, and general documentaation can be 
found at 
[chaos-toolkit]: https://github.com/chaostoolkit/


## Prerequisites

To run these experiments you will need the [Chaos Toolkit CLI][chaos-toolkit] >= 0.3.0,
Python3+, and Pip installed.
[chaos-toolkit]: https://github.com/chaostoolkit/chaostoolkit

Install basic dependencies:

```shell
sudo yum install -y python3 \
wget \
git \
curl \
https://centos7.iuscommunity.org/ius-release.rp
```

Create and access virtual environment as follows:

```shell
python3 -m venv ~/.venvs/chaostk
source  ~/.venvs/chaostk/bin/activate
```

Once instide virtual environment you will also need to install the 
[chaostoolkit-aws][chaostk] extension:

```shell
(chaostk) $ pip install -U chaostoolkit-aws
(chaostk) $ pip install -U chaostoolkit-reporting
```

Verify installation by running:

```shell
(chaostk) $ chaos --version 
```
[chaosk8s]: https://github.com/chaostoolkit/chaostoolkit-aws

## Running Experiments to Discover Weaknesses

Once you have the prerequisites and applicatin url in place, you can now edit
the experiment.json file to fill in 'probe' URL and/or 'steady-state' section
and run each experiment as follows:

```shell
(chaostk) $ chaos run experiment.json
```
## Reporting

Run reports and export findings to html and pdf format:

```shell
(chaostk) $ chaos report --export-format=html5 chaos-report.json report.html
(chasotk) $ chaos report --export-format=pdf chaos-report.json report.pdf

```
## Discover and Init

To run toolkit basic experiments without  against the `before` conditions use
the Chaos Toolkit CLI 'Discover' and 'init' options. Follow prompts after each
command.

```shell
(venv) $ chaos discover
(venv) $ chaos init
```


# chaos-test

